import { Injectable, EventEmitter } from '@angular/core';
import { TransportVehicle, UserRating } from "../../models/models"
/*
  Generated class for the SharedProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SharedProvider {

  vehicleTap = new EventEmitter<TransportVehicle>()
  userTripTap = new EventEmitter<UserRating>()
  
  constructor() {
    console.log('Hello SharedProvider Provider');
  }

}
