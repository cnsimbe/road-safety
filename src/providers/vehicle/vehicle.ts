import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs/Rx"
import { TransportType, TransportVehicle } from "../../models/models"
/*
  Generated class for the VehicleProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class VehicleProvider {

  vehicle_types = new BehaviorSubject<TransportType[]>([])
  constructor(public http: HttpClient) {
    console.log('Hello VehicleProvider Provider');
  	this.http.get<TransportType[]>('assets/data/vehicle_types.json')
  	.do(r=>console.log(r))
  	.subscribe(t=>this.vehicle_types.next(t))
  }


  searchVehicle(search_text?:string):Observable<TransportVehicle[]> {
  	return this.http.get<TransportVehicle[]>('assets/data/vehicle_list.json')
  }

  getVehicleById(id:number):Observable<TransportVehicle> {
    return this.searchVehicle().map(vhs=>vhs.find(vh=>vh.id==id))
  }

  getVehicleByVehicleId(vehicle_id:string):Observable<TransportVehicle> {
    return this.searchVehicle().map(vhs=>vhs.find(vh=>vh.vehicle_id==vehicle_id))
  }

}
