import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Rx"
import { delay } from "rxjs/operator/delay"
import { Storage } from '@ionic/storage';
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {

  private _isLogin:boolean;
  private set isLogin (v:boolean){
    this.storage.set("login",String(v))
    this._isLogin = v
  };
  private get isLogin (){
    return this._isLogin
  };
  constructor(private storage:Storage,public http: HttpClient) { 
  }

  isInit():Promise<any> {
    return this.storage.get("login")
    .then(loginString=>{
      if(loginString=="true")
        this.isLogin = true
      else
        this.isLogin =  false;
    }) 
  }

  sendVerificationCode(phone_number:string):Observable<any> {
  	return Observable.of(null).delay(1000)
  }


  verifyCode(phone_number:string,code:string):Observable<any> {
  	return Observable.of(null).delay(1000)
    .finally(()=>this.isLogin=true)
  }

  logout():Observable<any> {
  	return Observable.of(null).delay(1000)
    .finally(()=>this.isLogin=false)
  }

  isLoggedin():Observable<boolean> {
    return Observable.of(this.isLogin)
  }

  getUser():Observable<any> {
    if(this._isLogin)
      return Observable.of(null).delay(1000)
    else
      return Observable.throw("you are not logged in")
  }
}
