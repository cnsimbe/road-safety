import { Injectable } from '@angular/core';
import { RatingProvider } from "../rating/rating"
import { CitizenProvider } from "../citizen/citizen"
import { AuthProvider } from "../auth/auth"
import { VehicleProvider } from "../vehicle/vehicle"
/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(public rating:RatingProvider,public auth:AuthProvider,public citizen:CitizenProvider,public vehicle:VehicleProvider) {
    console.log('Hello ApiProvider Provider');
  }

}
