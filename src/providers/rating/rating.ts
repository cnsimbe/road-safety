import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserRating, TransportVehicle } from "../../models/models"
import { Observable, BehaviorSubject } from "rxjs/Rx"
import { forkJoin } from "rxjs/observable/forkJoin"
import { VehicleProvider } from "../vehicle/vehicle"
/*
  Generated class for the RatingProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RatingProvider {

  constructor(public http: HttpClient, private vehicleAPI:VehicleProvider) {
    console.log('Hello RatingProvider Provider');
  }

  getUserRating(id:number):Observable<UserRating> {
  	return this.searchUserRating()
    .map(ratings=>ratings.find(rating=>rating.id==id))
  }

  searchUserRating(search_text?:string):Observable<UserRating[]> {
  	return Observable.forkJoin<any>([this.http.get<UserRating[]>('assets/data/user_rating_list.json')
  		, this.vehicleAPI.searchVehicle()
  		]).map(results=>{
  			let ratings = results[0]
  			let vehicles = results[1] as any[]

  			return ratings.map(r=>{
  				let vh = vehicles.find(v=>v.id==r.vehicle_id)
  				r.vehicle = vh
  				return r
  			})
  		})
  }

  ratingVehicle(vehicle:TransportVehicle,rating:UserRating):Observable<UserRating>{
    return this.getUserRating(rating.id)
  }

}
