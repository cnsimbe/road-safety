import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddNewRatingPage } from './add-new-rating';
import { ComponentsModule } from "../../components/components.module"
@NgModule({
  declarations: [
    AddNewRatingPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(AddNewRatingPage),
  ],
})
export class AddNewRatingPageModule {}
