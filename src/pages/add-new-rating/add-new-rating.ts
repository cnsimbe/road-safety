import { Component, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, ViewController, Slides, NavController, NavParams } from 'ionic-angular';
import { FormComponent } from "../../models/interface"
import { TransportVehicle, UserRating } from "../../models/models"
import { ApiProvider } from "../../providers/api/api"
import { Subscription } from "rxjs/Rx"
/**
 * Generated class for the AddNewRatingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-new-rating',
  templateUrl: 'add-new-rating.html',
})
export class AddNewRatingPage implements OnDestroy{

  subs:Subscription;
  formSubs:Subscription = new Subscription()
  @ViewChild('slides') slides: Slides;
  ratingInfo:FormComponent;
  vehicleInfo:FormComponent;
  tripInfo:FormComponent;

  @ViewChild('ratingInfo') set _ratingInfo (comp:FormComponent){
  	this.ratingInfo = comp;
  	let form = comp.form
    this.formSubs.add(form.statusChanges.debounceTime(500).subscribe(v=>this.onFormUpdate()))
  };
  @ViewChild('vehicleInfo') set _vehicleInfo (comp:FormComponent){
  	this.vehicleInfo = comp;
  	let form = comp.form
  	this.formSubs.add(form.statusChanges.debounceTime(500).subscribe(v=>this.onFormUpdate()))
  };
  @ViewChild('tripInfo') set _tripInfo (comp:FormComponent){
  	this.tripInfo = comp;
  	let form = comp.form
  	this.formSubs.add(form.statusChanges.debounceTime(500).subscribe(v=>this.onFormUpdate()))
  };
  constructor(private api:ApiProvider,private viewCtlr:ViewController,public navCtrl: NavController, public navParams: NavParams) {

  }

  ngOnDestroy() {
    this.formSubs.unsubscribe()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewRatingPage');
  }

  ionViewDidEnter() {
    this.subs = new Subscription()
    this.subs.add(
    this.slides.ionSlideDidChange
      .finally(()=>console.log("slides unsubscribed"))
    	.subscribe(()=>this.onFormUpdate())
      )
   	this.slides.onlyExternal = true
  }

  ionViewWillLeave() {
    this.subs.unsubscribe()
  }

  ionViewWillEnter() {
    
    let id = this.navParams.get("id")
    if(id)
    {
      this.vehicleInfo.form.setValue({id:id,vehicle_type_id:null,vehicle_id:null})
      this.api.vehicle.getVehicleById(id)
      .take(1)
      .subscribe(vehicle=>{
        let value 
        this.vehicleInfo.form.setValue({id:id,vehicle_type_id:vehicle.vehicle_type_id,vehicle_id:vehicle.vehicle_id})
      })
    }
  }

  onFormUpdate():boolean {
  	let index = this.slides.getActiveIndex();
  	if(index==0)
  	{
      this.slides.lockSwipeToNext(!this.vehicleInfo.form.valid)
  		return this.vehicleInfo.form.valid
  	}
  	else if(index==1)
  	{
  		this.slides.lockSwipeToPrev(false)
  		this.slides.lockSwipeToNext(!this.tripInfo.form.valid)
  		return this.tripInfo.form.valid
  	}
  	else
    {
      this.slides.lockSwipeToPrev(false)
  		return this.ratingInfo.form.valid 
    }
  }


  next() {
    if(!this.onFormUpdate())
      return
  	let index = this.slides.getActiveIndex();
  	if(index!=2)
  		this.slides.slideNext()
  	else
  	{
  		//save rating info
      let vehicle = this.vehicleInfo.form.value as TransportVehicle
      let tripInfo = this.tripInfo.form.value as UserRating
      let rating = this.ratingInfo.form.value as UserRating
      Object.keys(tripInfo).forEach(k=>rating[k]=tripInfo[k])

      console.log(vehicle)
      console.log(rating)
      this.api.rating.ratingVehicle(vehicle,rating)
      .take(1)
      .subscribe(r=>{
        console.log(r)
        this.dismiss()
      })
  	}

  }

  back() {
  	let index = this.slides.getActiveIndex();
  	if(index!=0)
  		this.slides.slidePrev()
  	else
      this.dismiss()
  }

  dismiss() {
    if(this.navCtrl.canGoBack())
        this.viewCtlr.dismiss()
      else
        this.navCtrl.setRoot('HomePage')
  }

}
