import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatingDetailPage } from './rating-detail';
import { PipesModule } from "../../pipes/pipes.module"
@NgModule({
  declarations: [
    RatingDetailPage,
  ],
  imports: [
  	PipesModule,
    IonicPageModule.forChild(RatingDetailPage),
  ],
})
export class RatingDetailPageModule {}
