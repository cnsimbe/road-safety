import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserRating } from "../../models/models"
import { ApiProvider } from "../../providers/api/api"
/**
 * Generated class for the RatingDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-rating-detail',
  templateUrl: 'rating-detail.html',
})
export class RatingDetailPage {

  rating:UserRating;
  constructor(private api:ApiProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
  	let id = this.navParams.get("id")
  	this.api.rating.getUserRating(id)
  	.take(1)
  	.subscribe(rating=>this.rating=rating)
    console.log('ionViewDidLoad RatingDetailPage');
  }

  back() {
    this.navCtrl.canGoBack() ?  this.navCtrl.pop() : this.navCtrl.setRoot('HomePage')
  }

}
