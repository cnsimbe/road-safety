import { Component, ViewChild } from '@angular/core';
import { IonicPage, ActionSheetController, Tab, NavController, NavParams } from 'ionic-angular';
import { UserRating, TransportVehicle } from "../../models/models"
import { SafefyInfoListComponent } from "../../components/safefy-info-list/safefy-info-list"
import { RecentTripListComponent } from "../../components/recent-trip-list/recent-trip-list"
import { SharedProvider } from "../../providers/shared/shared"
import { Subscription } from "rxjs/Rx"
import { Push, PushObject, PushOptions, IOSPushOptions, AndroidPushOptions } from '@ionic-native/push';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  tab1 =  {
  		comp: SafefyInfoListComponent,
  		title: "Search Safety Info"
  }
  tab2 =  {
  		comp: RecentTripListComponent,
  		title: "Your Recent trips"
  }
  subs = new Subscription()
  constructor(private push:Push,private actionCtlr:ActionSheetController,private sharedService:SharedProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

    let push  = this.push.init({
        android:<AndroidPushOptions>{icon:'icon',senderID:"1083363070900",forceShow:true,vibrate:true},
        ios: <IOSPushOptions>{clearBadge:true,alert:true}
      })
    this.push.hasPermission()
    .catch(err=>false)
    .then(isPermit=>{
      push.on('registration')
      .subscribe(reg=>console.log(reg))


      push.on('notification')
      .subscribe(data=>console.log(data))  
    })
  }

  addRating() {
    this.navCtrl.push('AddNewRatingPage')
  }

  ionViewWillEnter() {
    this.subs = new Subscription()
    this.subs.add(this.sharedService.userTripTap
    .subscribe(trip=>{
      console.log("trip tapped", trip.id)
      this.showTripOptions(trip)
    }))

    this.subs.add(this.sharedService.vehicleTap.subscribe(vehicle=>{
      console.log("vehicle tapped", vehicle.id)
      this.showVehicleOptions(vehicle)
    }))
  }

  ionViewWillLeave() {
    console.log("unsubscribed")
    this.subs.unsubscribe()
  }

  showVehicleOptions(vehicle:TransportVehicle) {
    let action = this.actionCtlr.create({title:"What do you want to do?",buttons:[
        {text:"Rate this vehicle", handler:()=>{this.navCtrl.push('AddNewRatingPage',{id:vehicle.id})}}
      ]})
    action.present()
  }

  showTripOptions(trip:UserRating) {
    let action = this.actionCtlr.create({title:"What do you want to do?",buttons:[
        {text:"View rating details", handler:()=>{this.navCtrl.push('RatingDetailPage',{id:trip.id})}},
        {text:"Rate this vehicle", handler:()=>{this.navCtrl.push('AddNewRatingPage',{id:trip.vehicle_id})}}
      ]})
    action.present()
  }
}
