import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api"
/**
 * Generated class for the LandingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  slides = [
    { img: "assets/about/3.jpg", title:"Be safe on the road"},
    { img: "assets/about/2.jpeg", title:"Choose good vehicle operators"},
    { img: "assets/about/1.jpg", title:"Rate & Contribute"}
  ]
  constructor(public loadCtlr:LoadingController,public api:ApiProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LandingPage');
  }

  next() {
  	this.navCtrl.push('SignInPage')
  }

  ionViewDidEnter() {
    this.api.auth.isLoggedin()
    .subscribe(login=>{
      if(!login)
        return
      let loading =   this.loadCtlr.create({content:"logging in"})
      loading.present()
      this.api.auth.getUser()
      .finally(()=>loading.dismiss())
      .subscribe(()=>this.navCtrl.setRoot("HomePage",null,{animate:true}),err=>console.log(err))
    },err=>console.log(err))
  }

}
