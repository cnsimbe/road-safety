import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, NavParams } from 'ionic-angular';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
/**
 * Generated class for the SigInPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sign-in',
  templateUrl: 'sign-in.html',
})
export class SignInPage {

  constructor(private fb:Facebook,private gplus:GooglePlus,public platform:Platform,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignInPage');
  }

  goToPhonePage() {
  	this.navCtrl.push('PhoneVerifyPage')
  }

  signIn(authProvider:string) {
    if(authProvider=='google')
      this.gplus.login({}).then(t=>{
        console.log(t)
        this.goToPhonePage()
      })
    else if(authProvider=='fb')
      this.fb.login(['email','public_profile']).then(t=>{
        console.log(t)
        this.goToPhonePage()
      })
    else
      this.goToPhonePage()
  }

  back() {
  	if(this.navCtrl.canGoBack())
        this.navCtrl.pop()
      else
        this.navCtrl.setRoot('LandingPage')
  }

}
