import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhoneVerifyPage } from './phone-verify';
import { ComponentsModule } from "../../components/components.module"
@NgModule({
  declarations: [
    PhoneVerifyPage,
  ],
  imports: [
  	ComponentsModule,
    IonicPageModule.forChild(PhoneVerifyPage),
  ],
})
export class PhoneVerifyPageModule {}
