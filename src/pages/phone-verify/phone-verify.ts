import { Component, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, LoadingController, ViewController, Slides, NavController, NavParams } from 'ionic-angular';
import { FormComponent } from "../../models/interface"
import { TransportVehicle, UserRating } from "../../models/models"
import { ApiProvider } from "../../providers/api/api"
import { Subscription } from "rxjs/Rx"
/**
 * Generated class for the PhoneVerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-phone-verify',
  templateUrl: 'phone-verify.html',
})
export class PhoneVerifyPage {

  subs:Subscription;
  formSubs:Subscription = new Subscription()
  @ViewChild('slides') slides: Slides;
  enterPhoneNumber:FormComponent;
  verifyCode:FormComponent;

  @ViewChild('enterPhoneNumber') set _enterPhoneNumber (comp:FormComponent){
  	this.enterPhoneNumber = comp;
  	this.enterPhoneNumber.onNext.subscribe(()=>this.next())
  	let form = comp.form
    this.formSubs.add(form.statusChanges.debounceTime(500).subscribe(v=>this.onFormUpdate()))
  };
  @ViewChild('verifyCode') set _verifyCode (comp:FormComponent){
  	this.verifyCode = comp;
  	this.verifyCode.onNext.subscribe(()=>this.next())
  	this.verifyCode.onBack.subscribe(()=>this.back())
  	let form = comp.form
  	this.formSubs.add(form.statusChanges.debounceTime(500).subscribe(v=>this.onFormUpdate()))
  };
  constructor(private loadingCtlr:LoadingController,private api:ApiProvider,private viewCtlr:ViewController,public navCtrl: NavController, public navParams: NavParams) {

  }

  ngOnDestroy() {
    this.formSubs.unsubscribe()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddNewRatingPage');
  }

  ionViewDidEnter() {
    this.subs = new Subscription()
    this.subs.add(
    this.slides.ionSlideDidChange
      .finally(()=>console.log("slides unsubscribed"))
    	.subscribe(()=>this.onFormUpdate())
      )
   	this.slides.onlyExternal = true
  }

  ionViewWillLeave() {
    this.subs.unsubscribe()
  }

  onFormUpdate():boolean {
  	let index = this.slides.getActiveIndex();
  	if(index==0)
  	{
  		this.slides.lockSwipeToNext(!this.enterPhoneNumber.form.valid)
  		return this.enterPhoneNumber.form.valid
  	}
  	else
  	{
  		this.slides.lockSwipeToPrev(false)
  		return this.verifyCode.form.valid 
  	}
  }


  next() {
  	if(!this.onFormUpdate())
  		return
  	let index = this.slides.getActiveIndex();
  	if(index==0)
  	{
      let entryPhoneValue = this.enterPhoneNumber.form.value
  		let phone_number = entryPhoneValue.phone_number
  		let loading = this.loadingCtlr.create({content:"Sending code..."})
  		loading.present()
  		this.api.auth.sendVerificationCode(phone_number)
  		.finally(()=>loading.dismiss())
  		.subscribe(()=>{
  			this.slides.slideNext()
  			this.verifyCode.form.setValue({phone_number:phone_number,code:null})
  		})
  	}
  	else if(index==1)
  	{
  		let phone_number = this.verifyCode.form.value.phone_number
  		let code = this.verifyCode.form.value.code
  		let loading = this.loadingCtlr.create({content:"Verifying..."})
  		loading.present()
  		this.api.auth.verifyCode(phone_number,code)
  		.finally(()=>loading.dismiss())
  		.subscribe(()=>this.navCtrl.setRoot("HomePage"))
  	}

  }

  back() {
  	let index = this.slides.getActiveIndex();
  	if(index!=0)
  		this.slides.slidePrev()
  	else
      this.dismiss()
  }

  dismiss() {
    if(this.navCtrl.canGoBack())
        this.viewCtlr.dismiss()
      else
        this.navCtrl.setRoot('SignInPage')
  }
}
