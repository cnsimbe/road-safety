import { Directive, Input, ElementRef, Renderer } from '@angular/core';
import { Range } from "ionic-angular"
/**
 * Generated class for the RangeSlideColorDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[slider]' // Attribute selector
})
export class RangeSlideColorDirective {

  leftElm:HTMLElement;
  rightElm:HTMLElement;
  @Input('slider') set _slider(slide:Range) {


  setTimeout(()=>{
	  let parent = (slide.getNativeElement() as HTMLElement).querySelector(".range-slider")
	  this.leftElm = parent.querySelector(".range-bar") as HTMLElement
	  this.leftElm.classList.add("slider-negtive")

	  this.rightElm = this.renderer.createElement(parent,"div")
	  this.rightElm.classList.add("range-bar")
	  this.rightElm.classList.add("slider-positive")
  },330)

  	slide.ionChange.subscribe(v=>this.update((slide.value||0)/slide.max))
  }

  update(value:number) {
		if(value<=0.5) {
			this.leftElm.classList.add("slider-negtive")

  			this.rightElm.style.setProperty("width", `50%`)
  			this.rightElm.classList.remove("active")
  		}
  		else {
  			this.leftElm.classList.remove("slider-negtive")

  			this.rightElm.classList.add("active")
  			this.rightElm.style.setProperty("width", `${(value-0.5)*100}%`)
  			
  		}
  }

  constructor(private renderer:Renderer) {

    console.log('Hello RangeSlideColorDirective Directive');
  }

}
