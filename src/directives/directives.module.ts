import { NgModule } from '@angular/core';
import { InputPhoneDirective } from './input-phone/input-phone';
import { RangeSlideColorDirective } from './range-slide-color/range-slide-color';
@NgModule({
	declarations: [InputPhoneDirective,
    RangeSlideColorDirective],
	imports: [],
	exports: [InputPhoneDirective,
    RangeSlideColorDirective]
})
export class DirectivesModule {}
