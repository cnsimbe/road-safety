import { Directive, NgZone, ChangeDetectorRef, Input } from '@angular/core';
import { TextInput } from "ionic-angular"
import  { NgForm, AbstractControl, Validators } from "@angular/forms"
import { parse, CountryCode, format, asYouType, isValidNumber } from 'libphonenumber-js'
/**
 * Generated class for the InputPhoneDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[input-phone]' // Attribute selector
})
export class InputPhoneDirective {

  country:CountryCode
  inputField:TextInput
  @Input('code') set _country(country:CountryCode){
  	console.log(country)
  	this.country = country
  	if(this.inputField)
  	{
  		this.inputField.ngControl.control["country"] = country
  		this.inputField.ngControl.control.updateValueAndValidity()
  	}
  }
  @Input('input-phone') set input_phone(textInput:TextInput) {
  	this.inputField = textInput;
  	textInput.ionBlur.subscribe(()=>{
      let control = textInput.ngControl.control
      let error = PhoneValidator(control)
      if(!error)
        textInput.setValue(format(control.value,this.country,'International'))
    })

    this.inputField.ngControl.valueChanges.subscribe(v=>{
      let value = this.inputField.value || ""
      value = value.replace(/^\s+/, '')
      if(value.startsWith("0"))
        value =  value.replace("0","")
      this.inputField.setValue(value)
    })

  	let fc = textInput.ngControl.control
  	fc["country"] = this.country
  	fc.setValidators([Validators.required, PhoneValidator])
  }
  constructor(private ngZone:NgZone,private changeRef:ChangeDetectorRef) {
    console.log('Hello InputPhoneDirective Directive');
  }

}


export function PhoneValidator(control:AbstractControl) {
	try
	{

		let p = parse(control.value,control["country"])
		let isValid = isValidNumber(p)
		if(isValid)
			return null
		else
			return {phone_valid : true}
	}catch(exp) {
		return {phone_valid : true} 
	}
	
}
