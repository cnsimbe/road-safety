import { EventEmitter } from "@angular/core"
import { NgForm } from "@angular/forms"
export interface FormComponent {
	form:NgForm
	onNext?:EventEmitter<any>;
	onBack?:EventEmitter<any>;
}