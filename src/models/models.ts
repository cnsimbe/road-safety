export class Citizen {
	id:number;
	user_id:number;
	phone:string;
}

export class TransportType {
	id:number;
	name;
}
export class TransportVehicle {
	id:number;
	vehicle_id:string;
	vehicle_type_id:number;
	summary_score:number;
	summary_score_text:string;
	rating_count:number;

}


export class UserRating {
	id:number;
	citizen_id:number;
	vehicle_id:number;
	vehicle:TransportVehicle;
	citizen:any;
	original_text:string;
	summary_score:number;
	summary_score_text:string;
	overall_score:number;
	overall_score_text:string;
	obey_traffic_sign:number;
	safe_speed:number;
	conductor_helmet:number;
	passenger_helmet:number;
	vehicle_mechanical_condition:number;
	safe_overtake:number;
	attention_on_road:number;
	had_accident:number;
	created_at:string;

	start_time:string;
	end_time:string;
	origin:any;
	destination:any;

}