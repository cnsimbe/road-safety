import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http"
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { IonicStorageModule } from '@ionic/storage';
import { Push } from '@ionic-native/push';

import { MyApp } from './app.component';
import { ConstantsProvider } from '../providers/constants/constants';
import { VehicleProvider } from '../providers/vehicle/vehicle';
import { RatingProvider } from '../providers/rating/rating';
import { CitizenProvider } from '../providers/citizen/citizen';
import { AuthProvider } from '../providers/auth/auth';
import { ApiProvider } from '../providers/api/api';
import { ComponentsModule } from "../components/components.module"
import { SharedProvider } from '../providers/shared/shared';
import { Globalization } from '@ionic-native/globalization';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    ComponentsModule,
    IonicStorageModule.forRoot({
      name: '__pactsp',
      driverOrder: ['sqlite', 'indexeddb','localstorage']
    }),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Push,
    StatusBar,
    SplashScreen,
    InAppBrowser,
    EmailComposer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ConstantsProvider,
    VehicleProvider,
    RatingProvider,
    CitizenProvider,
    AuthProvider,
    ApiProvider,
    SharedProvider,
    Globalization,
    GooglePlus,
    Facebook
  ]
})
export class AppModule {}
