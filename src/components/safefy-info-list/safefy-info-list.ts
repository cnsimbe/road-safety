import { Component, EventEmitter, Output } from '@angular/core';
import { ApiProvider } from "../../providers/api/api"
import { SharedProvider } from "../../providers/shared/shared"
import { TransportVehicle } from "../../models/models"
/**
 * Generated class for the SafefyInfoListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'safefy-info-list',
  templateUrl: 'safefy-info-list.html'
})
export class SafefyInfoListComponent {

  @Output('vehicleSelect') vehicleSelect = new EventEmitter<TransportVehicle>()
  searchText: string;
  vehicleList:TransportVehicle[];
  constructor(private api:ApiProvider, private sharedService:SharedProvider) {
    console.log('Hello SafefyInfoListComponent Component');
    this.clearSearch()
    
  }

  searchVehicles(){
  	this.api.vehicle.searchVehicle(this.searchText)
  	.take(1)
  	.subscribe(vl=>this.vehicleList=vl)
  }

  clearSearch() {
  	this.searchText = null
  	this.searchVehicles()
  }

  onVehicleSelect(vehicle:TransportVehicle) {
    this.vehicleSelect.emit(vehicle)
    this.sharedService.vehicleTap.emit(vehicle)
  }

}
