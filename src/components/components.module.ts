import { NgModule } from '@angular/core';
import { EnterPhoneNumberComponent } from './enter-phone-number/enter-phone-number';
import { EnterVerificationCodeComponent } from './enter-verification-code/enter-verification-code';
import { SafefyInfoListComponent } from './safefy-info-list/safefy-info-list';
import { RecentTripListComponent } from './recent-trip-list/recent-trip-list';
import { AddRatingVehicleInfoComponent } from './add-rating-vehicle-info/add-rating-vehicle-info';
import { AddRatingTripInfoComponent } from './add-rating-trip-info/add-rating-trip-info';
import { AddRatingRatingInfoComponent } from './add-rating-rating-info/add-rating-rating-info';
import { DirectivesModule } from "../directives/directives.module"
import { IonicModule } from "ionic-angular"
import { PipesModule } from "../pipes/pipes.module"
import { MenuComponent } from './menu/menu';
@NgModule({
    entryComponents: [EnterPhoneNumberComponent,
    EnterVerificationCodeComponent,
    SafefyInfoListComponent,
    RecentTripListComponent,
    AddRatingVehicleInfoComponent,
    AddRatingTripInfoComponent,
    AddRatingRatingInfoComponent],


	declarations: [EnterPhoneNumberComponent,
    EnterVerificationCodeComponent,
    SafefyInfoListComponent,
    RecentTripListComponent,
    AddRatingVehicleInfoComponent,
    AddRatingTripInfoComponent,
    AddRatingRatingInfoComponent,
    MenuComponent],
	imports: [IonicModule,PipesModule,DirectivesModule],

    
	exports: [EnterPhoneNumberComponent,
    EnterVerificationCodeComponent,
    SafefyInfoListComponent,
    RecentTripListComponent,
    AddRatingVehicleInfoComponent,
    AddRatingTripInfoComponent,
    AddRatingRatingInfoComponent,
    MenuComponent]
})
export class ComponentsModule {}
