import { Component } from '@angular/core';
import { LoadingController, AlertController, NavController } from "ionic-angular"
import { ApiProvider } from "../../providers/api/api"
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
/**
 * Generated class for the MenuComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'menu-home',
  templateUrl: 'menu.html'
})
export class MenuComponent {

  feedbackEmail = "cnsimbe7@gmail.com"
  constructor(private alertCtlr:AlertController,private emailCompose:EmailComposer,private browser:InAppBrowser,private api:ApiProvider,private navCtlr:NavController, private loadCtlr:LoadingController) {
    console.log('Hello MenuComponent Component');
  }

  logout() {
  	let loading = this.loadCtlr.create({content:"Logging out..."})
  	loading.present()
  	this.api.auth.logout()
  	.finally(()=>loading.dismiss())
  	.subscribe(()=>this.navCtlr.setRoot('LandingPage'))
  }

  openAbout() {
    this.browser.create("https://patriotagenda.com","_system")
  }

  openFeedback() {
    this.emailCompose.open({subject:"Feedback on Citizen Traffic Safety",to:this.feedbackEmail})
    .catch(err=>{
      let alert = this.alertCtlr.create({title:"Error opening email",message:`You can send an email to ${this.feedbackEmail} for any feedback on the platform`})
      alert.present()
    })
  }

}
