import { Component, EventEmitter, ViewChild } from '@angular/core';
import  { NgForm } from "@angular/forms"
import { FormComponent } from "../../models/interface"
import { TextInput } from "ionic-angular"
import * as country_data from "country-data-list"
import { format } from "libphonenumber-js"
import { Globalization } from '@ionic-native/globalization';
/**
 * Generated class for the EnterPhoneNumberComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'enter-phone-number',
  templateUrl: 'enter-phone-number.html'
})
export class EnterPhoneNumberComponent implements FormComponent{
	@ViewChild('form') form:NgForm;
	  onNext = new EventEmitter(true)
  	onBack;
  	info:{ phone_number:string, country_code:string }= <any>{}
  	countries = country_data.countries.all
  	constructor(private globalisation:Globalization) {

      this.globalisation.getLocaleName()
      .then(locale=>{
        let code = this.getCountryCodeFromLocale(locale.value)
        let country = this.countries.find(c=>c.alpha2==code)
        if(country)
          this.info.country_code = country.alpha2

      }).catch(err=>console.log(err))
    	console.log('Hello EnterPhoneNumberComponent Component');
  	}

    getCountryCodeFromLocale(locale:string):string {
      locale = locale || ""
      let splits = locale.split("-")
      return splits[splits.length-1]
    }

  	countryIndex(index,country) {
  		return index;
  	}


    onClickNext() {
      this.info.phone_number = format(this.info.phone_number,<any>this.info.country_code,"International")
      console.log(this.info.phone_number)
      this.onNext.emit()
    }



}
