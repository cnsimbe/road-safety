import { Component, EventEmitter, Output } from '@angular/core';
import { NavController } from "ionic-angular"
import { ApiProvider } from "../../providers/api/api"
import { UserRating } from "../../models/models"
import { SharedProvider } from "../../providers/shared/shared"
/**
 * Generated class for the RecentTripListComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'recent-trip-list',
  templateUrl: 'recent-trip-list.html'
})
export class RecentTripListComponent {
  @Output('tripSelect') tripSelect = new EventEmitter<UserRating>()
  searchText: string;
  ratingList:UserRating[];
  constructor(private api:ApiProvider, private sharedService:SharedProvider, private navCtrl:NavController) {
    console.log('Hello RecentTripListComponent Component');
    this.clearSearch()
    
  }

  searchTrips(){
  	this.api.rating.searchUserRating(this.searchText)
  	.take(1)
  	.subscribe(vl=>this.ratingList=vl)
  }

  clearSearch() {
  	this.searchText = null
  	this.searchTrips()
  }

  onTripSelect(trip:UserRating) {
    this.tripSelect.emit(trip)
    this.sharedService.userTripTap.emit(trip)
  }
}
