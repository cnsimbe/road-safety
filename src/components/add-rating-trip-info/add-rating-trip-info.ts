import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { TransportType } from "../../models/models"
import { ApiProvider } from "../../providers/api/api"
import  { NgForm } from "@angular/forms"
import { FormComponent } from "../../models/interface"
/**
 * Generated class for the AddRatingTripInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'add-rating-trip-info',
  templateUrl: 'add-rating-trip-info.html'
})
export class AddRatingTripInfoComponent implements FormComponent, OnInit{
  @ViewChild('form') form:NgForm;
  onNext;
  onBack;
  maxDate
  minDate
  info:{ start_time:string, end_time:string, origin:string, destination:string }= <any>{}
  constructor(private api:ApiProvider) {
    console.log('Hello AddRatingTripInfoComponent Component');
    
  }

  

  ngOnInit() {
    this.maxDate = new Date()
    this.maxDate.setTime(this.maxDate.getTime() + 6*365*24*3600*1000)
    
    this.minDate = new Date()
    this.minDate.setTime(this.minDate.getTime() - 6*365*24*3600*1000)
  }




}

