import { Component, EventEmitter, ViewChild } from '@angular/core';
import { TransportType } from "../../models/models"
import { ApiProvider } from "../../providers/api/api"
import  { NgForm } from "@angular/forms"
import { FormComponent } from "../../models/interface"
/**
 * Generated class for the AddRatingVehicleInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'add-rating-vehicle-info',
  templateUrl: 'add-rating-vehicle-info.html'
})
export class AddRatingVehicleInfoComponent implements FormComponent{
  @ViewChild('form') form:NgForm;
  onNext;
  onBack;
  vehicle_types:TransportType[];
  info:{ id:number,vehicle_id:string, vehicle_type_id:string }= <any>{}
  constructor(private api:ApiProvider) {
    console.log('Hello AddRatingVehicleInfoComponent Component');
    this.api.vehicle.vehicle_types.subscribe(t=>this.vehicle_types=t)
  }


}
