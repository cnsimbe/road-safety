import { Component,ViewChild, EventEmitter } from '@angular/core';
import { FormComponent } from "../../models/interface"
import { RatingInfoListPipe } from "../../pipes/rating-info-list/rating-info-list"
import { NgForm } from "@angular/forms"
/**
 * Generated class for the AddRatingRatingInfoComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'add-rating-rating-info',
  templateUrl: 'add-rating-rating-info.html'
})
export class AddRatingRatingInfoComponent implements FormComponent{
  minFactors = 3;
  formValid:string;
  @ViewChild('form') form:NgForm;
  onNext;
  onBack;
  models:Array<any> = this.ratingInfoList.transform()

  constructor(private ratingInfoList:RatingInfoListPipe) {
    console.log('Hello AddRatingRatingInfoComponent Component');

  }

  updateForm() {
  	let isValid = this.models.filter(m=>m.type=='range' && m.selected==true && m.value!=5).length>=this.minFactors;
  	this.form.setValue({"valid":isValid ? "true" : null})
  }


  
}
