import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SortArrayPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'sortArray',
})
export class SortArrayPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(array:any[],field:string,direction?:string):any[] {
  	direction =  direction || 'asc'
  	array = array || []
	return array.sort(function(a, b){
	 var nameA=a[field].toLowerCase(), nameB=b[field].toLowerCase();
	 if (nameA < nameB) //sort string ascending
	  return direction=='asc' ? -1 : 1;
	 if (nameA > nameB)
	  return direction=='asc' ? 1 : -1;
	 return 0; //default return value (no sorting)
	});
  }
}
