import { Pipe, PipeTransform } from '@angular/core';
import { UserRating } from "../../models/models"
/**
 * Generated class for the RatingInfoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'ratingInfo',
})
export class RatingInfoPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  private fieldMap = {	
  		obey_traffic_sign:'Obey Traffic Sign',
  		safe_speed:'Safe driving speed',
  		passenger_helmet:'Passenger helmet',
  		vehicle_mechanical_condition:'Mechanical condition',
  		safe_overtake:'Safe overtaking',
  		attention_on_road:'Attention while driving',
  		had_accident:'Involved in accident',
  	}
  transform(rating: UserRating):any[] {
    rating = rating || <UserRating>{}
    return Object.keys(this.fieldMap)
    .filter(k=>rating[k]!=undefined&&rating[k]!=null)
    .map(k=>{
      let rtxt = k=='had_accident' ? rating[k] : `${rating[k]}%`
    	return {name:this.fieldMap[k],rating:rating[k],rating_text:rtxt,field:k}
    })
  }
}
