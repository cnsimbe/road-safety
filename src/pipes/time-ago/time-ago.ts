import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TimeAgoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'timeAgo',
})
export class TimeAgoPipe implements PipeTransform {
  /**
   * returns time ago
   */
  transform(value: string|number|Date):string {
    let d:Date;
    if(typeof value=='string' || typeof value=='number')
    	d = new Date(<any>value)
    else if(value instanceof Date)
    	d = value
    else
    	return undefined

    let now = new Date()

	let seconds = Math.round(Math.abs((now.getTime() - d.getTime())/1000));
	
	let minutes = Math.round(Math.abs(seconds / 60));
	let hours = Math.round(Math.abs(minutes / 60));
	let days = Math.round(Math.abs(hours / 24));
	let months = Math.round(Math.abs(days/30.416));
	let years = Math.round(Math.abs(days/365));
	if (seconds <= 45) {
		return 'a few seconds ago';
	} else if (seconds <= 90) {
		return 'a minute ago';
	} else if (minutes <= 45) {
		return minutes + ' minutes ago';
	} else if (minutes <= 90) {
		return 'an hour ago';
	} else if (hours <= 22) {
		return hours + ' hours ago';
	} else if (hours <= 36) {
		return 'a day ago';
	} else if (days <= 25) {
		return days + ' days ago';
	} else if (days <= 45) {
		return 'a month ago';
	} else if (days <= 345) {
		return months + ' months ago';
	} else if (days <= 545) {
		return 'a year ago';
	} else { // (days > 545)
		return years + ' years ago';
	}

  }
}
