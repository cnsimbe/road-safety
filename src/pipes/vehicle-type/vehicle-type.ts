import { Pipe, PipeTransform } from '@angular/core';
import { ApiProvider } from "../../providers/api/api"
/**
 * Generated class for the VehicleTypePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'vehicleType',
})
export class VehicleTypePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  constructor(private api:ApiProvider){}
  transform(type_id:number):string {
  	let vt = this.api.vehicle.vehicle_types.getValue().find(vt=>vt.id==type_id)
    return vt ? vt.name : undefined
  }
}
