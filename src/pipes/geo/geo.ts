import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the GeoPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'geo',
})
export class GeoPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(point:any):string {
    return point ? point.name : undefined
  }
}
