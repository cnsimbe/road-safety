import { NgModule } from '@angular/core';
import { RatingInfoPipe } from './rating-info/rating-info';
import { TimeAgoPipe } from './time-ago/time-ago';
import { VehicleTypePipe } from './vehicle-type/vehicle-type';
import { GeoPipe } from './geo/geo';
import { SortArrayPipe } from './sort-array/sort-array';
import { RatingInfoListPipe } from './rating-info-list/rating-info-list';
@NgModule({
	declarations: [RatingInfoPipe,
    TimeAgoPipe,
    VehicleTypePipe,
    GeoPipe,
    SortArrayPipe,
    RatingInfoListPipe],
	imports: [],

	exports: [RatingInfoPipe,
    TimeAgoPipe,
    VehicleTypePipe,
    GeoPipe,
    SortArrayPipe,
    RatingInfoListPipe],

    providers: [RatingInfoListPipe]
})
export class PipesModule {}
