import { Pipe, Injectable , PipeTransform } from '@angular/core';
import { UserRating } from "../../models/models"
/**
 * Generated class for the RatingInfoListPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Injectable()
@Pipe({
  name: 'ratingInfoList',
})
export class RatingInfoListPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  private fieldMap = {	
  		obey_traffic_sign:'Obey Traffic Sign',
  		safe_speed:'Safe driving speed',
  		passenger_helmet:'Passenger helmet',
  		vehicle_mechanical_condition:'Mechanical condition',
  		safe_overtake:'Safe overtaking',
  		attention_on_road:'Attention while driving',
  		had_accident:'Involved in accident',
  	}
  transform(rating?):any[] {
    rating = rating || this.fieldMap
    return Object.keys(this.fieldMap)
    .filter(k=>rating[k]!=undefined&&rating[k]!=null)
    .map(k=>{
    	let type = k=='had_accident' ? 'check' : 'range'
      	return {name:this.fieldMap[k],type:type,value:5}
    })
  }
}
